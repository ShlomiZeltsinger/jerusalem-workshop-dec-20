pragma solidity 0.4.18;

contract Demo{ // Address 0xf12b5dd4ead5f743c6baa640b0216200e89b60da name space for all the functions, vaiables, class
	
	uint public number;

	mapping(address=>string) public names;
	
	function changeNumber(uint _input) public{
		number = _input;
	}

	function register(string _input) public{
		//if(names[msg.sender] == 0x00) 
		names[msg.sender] = _input;
 	}
}