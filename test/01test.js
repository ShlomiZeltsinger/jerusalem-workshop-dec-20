const Demo = artifacts.require("Demo");

const expect = require("chai").expect;

contract("Testing the Demo contract", function(){
	describe("Deploy and grab the Demo contract", function(){
		it("Deploying and grabing", function(){
			return Demo.new().then(res=>{
				mySmarctContract = res;
			});
		});

		it("Deploying and grabing", function(){
			return Demo.new().then(res=>{
				newBuilding = res;
			});
		});
	});

	describe("For newBuilding check var and functions", function(){
		it("call chaneNumber", function(){
			return newBuilding.changeNumber(9);
		});
	
	it("checks number to be 9", function(){
		return newBuilding.number().then(res=>{
			expect(res.toNumber()).to.be.equal(9);
		});
	});

	it("checks number to be 9", function(){
		return mySmarctContract.number().then(res=>{
		
	expect(res.toNumber()).to.be.equal(0);
			}); 
		});
	});

	describe("Trying the new functions", function(){
		it("Register new user", function(){
			return newBuilding.register("shlomi", {from: "0xf17f52151ebef6c7334fad080c5704d77216b732"});
		});

		it("should be equal shlomi", function(){
			return newBuilding.names("0xf17f52151ebef6c7334fad080c5704d77216b732").then(res=>{
				expect(res.toString()).to.be.equal("shlomi");
			});
		});




	});

});